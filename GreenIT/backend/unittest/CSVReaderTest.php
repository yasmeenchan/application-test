<?php

namespace Tests;

use CSVReader;
use Tests\TestCase;

/**
 * Class CSVReaderTest.
 *
 * @covers \CSVReader
 */
final class CSVReaderTest extends TestCase
{
    private CSVReader $cSVReader;

    private mixed $filename;

    private mixed $delimiter;

    private mixed $rowDelimiter;

    private mixed $uniqueId;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->filename = null;
        $this->delimiter = null;
        $this->rowDelimiter = null;
        $this->uniqueId = null;
        $this->cSVReader = new CSVReader($this->filename, $this->delimiter, $this->rowDelimiter, $this->uniqueId);
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown(): void
    {
        parent::tearDown();

        unset($this->cSVReader);
        unset($this->filename);
        unset($this->delimiter);
        unset($this->rowDelimiter);
        unset($this->uniqueId);
    }

    public function testGetAllRecord(): void
    {
        /** @todo This test is incomplete. */
        $this->markTestIncomplete();
    }

    public function testSaveRecord(): void
    {
        /** @todo This test is incomplete. */
        $this->markTestIncomplete();
    }

    public function testUpdateRecord(): void
    {
        /** @todo This test is incomplete. */
        $this->markTestIncomplete();
    }

    public function testDeleteRecord(): void
    {
        /** @todo This test is incomplete. */
        $this->markTestIncomplete();
    }
}
