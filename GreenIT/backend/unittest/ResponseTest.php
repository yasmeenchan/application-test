<?php

namespace Tests;

use Response;
use Tests\TestCase;

/**
 * Class ResponseTest.
 *
 * @covers \Response
 */
final class ResponseTest extends TestCase
{
    private Response $response;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        parent::setUp();

        /** @todo Correctly instantiate tested object to use it. */
        $this->response = new Response();
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown(): void
    {
        parent::tearDown();

        unset($this->response);
    }

    public function testJsonResponse(): void
    {
        /** @todo This test is incomplete. */
        $this->markTestIncomplete();
    }
}
