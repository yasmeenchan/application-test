<?php

namespace Tests\Unit;

use Api;
use Tests\TestCase;

/**
 * Class ApiTest.
 *
 * @covers \Api
 */
final class ApiTest extends TestCase
{
    private Api $api;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->api = new Api();
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown(): void
    {
        parent::tearDown();

        unset($this->api);
    }

    public function testGetAllRecord(): void
    {
        /** @todo This test is incomplete. */
        $this->markTestIncomplete();
    }

    public function testSaveRecordPost(): void
    {
        /** @todo This test is incomplete. */
        $this->markTestIncomplete();
    }

    public function testUpdateRecordPost(): void
    {
        /** @todo This test is incomplete. */
        $this->markTestIncomplete();
    }

    public function testDeleteRecordPost(): void
    {
        /** @todo This test is incomplete. */
        $this->markTestIncomplete();
    }
}

?>
