
# GreenIT Application Challenge

 - This Project describes about the simple CRUD operations to the CSV file.


## Technology Requirement

Angular 13

PHP 8.0

## API Reference

https://documenter.getpostman.com/view/2318035/2s7YYvZMXh



## Authors

- Yasmeen


## Screenshots

![App Screenshot](http://localhost:4200/)

